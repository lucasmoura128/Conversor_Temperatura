#include <iostream>
#include "conversor.h"

using namespace std;

int main()
{

	float celsius;

	Conversor *conv = new Conversor();

	float resultado;

    cout<<"Conversor de temperatura basico"<<endl;
    cout<<"Digite o valor em celsius:"<<endl;
    cin>>celsius;
    resultado = conv->celsiusToFahrenheit(celsius);
	cout<<"Seu valor em Fahrenheit é "<<resultado<<endl;
	resultado = conv->celsius_kelvin(celsius);
	cout<<"Seu valor em Celsius é "<<resultado<<endl;

	delete conv;

    return 0;
}    
